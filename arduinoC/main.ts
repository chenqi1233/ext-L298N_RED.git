//% color="#B8860B" iconWidth=50 iconHeight=40
namespace L298N{
  
    //% block="L298N Motor Init Board[BOARD]EAPin[E1]M1Pin[M1]M1Pin[M2]EBPin[E2]M2Pin[M3]M2Pin[M4]   " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD" 
    //% E1.shadow="dropdown" E1.options="E1" 
    //% M1.shadow="dropdown" M1.options="M1"
    //% M2.shadow="dropdown" M2.options="M2"
    //% E2.shadow="dropdown" E2.options="E2"   
    //% M3.shadow="dropdown" M3.options="M3"
    //% M4.shadow="dropdown" M4.options="M4"
    export function L298NInit(parameter: any, block: any) {      
     let eA=parameter.E1.code;  
     let m1=parameter.M1.code;
     let m2=parameter.M2.code;
     let eB=parameter.E2.code;  
     let m3=parameter.M3.code;
     let m4=parameter.M4.code;
     let board=parameter.BOARD.code;
     
     Generator.addInclude("L298N","#include <L298N.h>");
     Generator.addObject(`L298N${board}` ,`L298N`,`tb${board}(${eA},${m1},${m2},${eB},${m3},${m4});`);
      
    }
    //% block="L298N Motor Board[BOARD]Motor[MOTOR]Dir[FR]Speed[SPEED]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    //% FR.shadow="dropdownRound" FR.options="FR"
    //% SPEED.shadow="range" SPEED.params.min="0" SPEED.params.max="255" SPEED.defl="200"
    export function L298NInit1(parameter: any, block: any) {        
     let board=parameter.BOARD.code;         
     let fr=parameter.FR.code;  
     let speed=parameter.SPEED.code;
     let m=parameter.MOTOR.code;

       Generator.addCode(`tb${board}.setSpeed(${m}, ${fr},${speed});`);
  
     }

    //% block="L298N Motor Board[BOARD]Motor[MOTOR]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    
    export function L298NInit2(parameter: any, block: any) {        
        let board=parameter.BOARD.code;         
        let m=parameter.MOTOR.code;
   
         Generator.addCode(`tb${board}.stop(${m});`);
 
           
        }
      
 
}