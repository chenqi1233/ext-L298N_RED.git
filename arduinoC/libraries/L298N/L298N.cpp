#include "L298N.h"
#include "Arduino.h"
 
L298N::L298N( int pwm1,int dir1,int dir2,int pwm2,int dir3,int dir4){
        _PWM1=pwm1;
        _DIR1=dir1;
        _DIR2=dir2;

        _PWM2=pwm2;
        _DIR3=dir3;
        _DIR4=dir4;
        pinMode(_DIR1, OUTPUT);   
        pinMode(_DIR2, OUTPUT); 
        pinMode(_DIR3, OUTPUT);   
        pinMode(_DIR4, OUTPUT); 
        
}


L298N::~L298N() {}
  

void    L298N::setSpeed(int Motor,int FR,int Speed){
    if(Motor == M1&&FR == CW){ 

        digitalWrite(_DIR1,HIGH);
        digitalWrite(_DIR2,LOW);

        analogWrite(_PWM1, Speed);
    }else if(Motor == M1&&FR == CCW){
      
        digitalWrite(_DIR1,LOW);
        digitalWrite(_DIR2,HIGH);
        analogWrite(_PWM1, Speed);
    }else if(Motor == M2&&FR == CW){
        digitalWrite(_DIR3,HIGH);
        digitalWrite(_DIR4,LOW);
        analogWrite(_PWM2, Speed);
    }else if(Motor ==M2&&FR  == CCW){
         digitalWrite(_DIR3,LOW);
        digitalWrite(_DIR4,HIGH);
        analogWrite(_PWM2, Speed);
    }else if(Motor == M3&&FR == CW){
        digitalWrite(_DIR1,HIGH);
        digitalWrite(_DIR2,LOW);
        analogWrite(_PWM1, Speed);

        digitalWrite(_DIR3,HIGH);
        digitalWrite(_DIR4,LOW);
        analogWrite(_PWM2, Speed);
    }else if(Motor == M3&&FR == CCW){
        digitalWrite(_DIR1,LOW);
        digitalWrite(_DIR2,HIGH);
        analogWrite(_PWM1, Speed);

        digitalWrite(_DIR3,LOW);
        digitalWrite(_DIR4,HIGH);
        analogWrite(_PWM2, Speed);
    }    
}
    
void    L298N::stop(int Motor){
    if(Motor==M1 ){      
        analogWrite(_PWM1,0);  
    }else if (Motor==M2){
        analogWrite(_PWM2,0);
    }else if (Motor==M3){
        analogWrite(_PWM1,0);
        analogWrite(_PWM2,0);
    }


}
